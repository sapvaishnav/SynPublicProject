import java.util.Random; 
import java.time.*;

public class PerformanceTest
{
	static int Powerof = 2, loop = 1000;
	static long Timefrom, Timeto;
	static int resultint = 1;
	static double resultdouble = 1;

	static double[][] TimeResult = new double[3][4];
	static int[] IntPositiveNumbers = new int[loop];
	static int[] IntNegativeNumbers = new int[loop];
	static double[] DoublePositiveNumbers = new double[loop];
	static double[] DoubleNegativeNumbers = new double[loop];
	static int[] IntPositiveNumbersResult = new int[loop];
	static int[] IntNegativeNumbersResult = new int[loop];
	static double[] DoublePositiveNumbersResult = new double[loop];
	static double[] DoubleNegativeNumbersResult = new double[loop];

	Random random = new Random();
     
	static void GenerateIntegerPositiveData() {
		for(int i = 0; i < loop; i++) {
			int randomInteger = new Random().nextInt();
			randomInteger = randomInteger % 1000;
			if(randomInteger > 0)
				IntPositiveNumbers[i] = randomInteger;
			else
				IntPositiveNumbers[i] = -1 * randomInteger;
		}
	}

	static void GenerateIntegerNegativeData() { 
		for(int i = 0; i < loop; i++) {
			int randomInteger = new Random().nextInt();
			randomInteger = randomInteger % 1000;
			if(randomInteger < 0)
				IntNegativeNumbers[i] = randomInteger;
			else
				IntNegativeNumbers[i] = -1 * randomInteger;
		}
	}

	static void GenerateDoublePositiveData() {
		for(int i = 0; i < loop; i++) {
			double random2 = new Random().nextDouble();
			double PositiveDouble = random2 % 1000;
			if(PositiveDouble > 0)
				DoublePositiveNumbers[i] = PositiveDouble;
			else
				DoublePositiveNumbers[i] = -1 * PositiveDouble;
		}
	}

	static void GenerateDoubleNegativeData() {
		for(int i = 0; i < loop; i++) {
			double random2 = new Random().nextDouble();
			double PositiveDouble = random2 % 1000;
			if(PositiveDouble < 0)
				DoubleNegativeNumbers[i] = PositiveDouble;
			else
				DoubleNegativeNumbers[i] = -1 * PositiveDouble;
		}
	}

	static void GenerateCaseData() {   
		GenerateIntegerPositiveData();
		GenerateIntegerNegativeData();
		GenerateDoublePositiveData();
		GenerateDoubleNegativeData();
	}

	public static void main(String []args) {

		GenerateCaseData();

		// Direct Method
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			IntPositiveNumbersResult[i] = IntPositiveNumbers[i] * IntPositiveNumbers[i];
		Timeto = System.nanoTime();
		TimeResult[0][0] = (Timeto - Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			IntNegativeNumbersResult[i] = IntNegativeNumbers[i] * IntNegativeNumbers[i]; 
		Timeto = System.nanoTime();
		TimeResult[0][1] = (Timeto - Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			DoublePositiveNumbersResult[i] = DoublePositiveNumbers[i] * DoublePositiveNumbers[i]; 
		Timeto = System.nanoTime();
		TimeResult[0][2] = (Timeto - Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			DoubleNegativeNumbersResult[i] = DoubleNegativeNumbers[i] * DoubleNegativeNumbers[i]; 
		Timeto = System.nanoTime();
		TimeResult[0][3] = (Timeto - Timefrom)/1e6;

         
		// Multiplication Using Iteration
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++) {
			powint(IntPositiveNumbers[i], Powerof);
			IntPositiveNumbersResult[i] = resultint;
			resultint = 1;
		}
		Timeto = System.nanoTime();
		TimeResult[1][0] = (Timeto - Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++) {
			powint(IntNegativeNumbers[i], Powerof);
			IntNegativeNumbersResult[i] = resultint;
			resultint = 1;
		}
		Timeto= System.nanoTime();
		TimeResult[1][1] = (Timeto - Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++) {
			powdouble(DoublePositiveNumbers[i], Powerof);
			DoublePositiveNumbersResult[i] = resultdouble;
			resultdouble = 1;
		}
		Timeto = System.nanoTime();
		TimeResult[1][2] = (Timeto - Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++) {
			powdouble(DoubleNegativeNumbers[i], Powerof);
			DoubleNegativeNumbersResult[i] = resultdouble;
			resultdouble = 1;
		}
		Timeto = System.nanoTime();
		TimeResult[1][3] = (Timeto - Timefrom)/1e6;
         

		// Using Math.pow Method
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			IntPositiveNumbersResult[i] = (int)Math.pow(IntPositiveNumbers[i], Powerof);
		Timeto = System.nanoTime();
		TimeResult[2][0] = (Timeto-Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			IntNegativeNumbersResult[i] = (int)Math.pow(IntNegativeNumbers[i], Powerof);
		Timeto = System.nanoTime();
		TimeResult[2][1] = (Timeto-Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			DoublePositiveNumbersResult[i] = (double)Math.pow(DoublePositiveNumbers[i], Powerof);
		Timeto = System.nanoTime();
		TimeResult[2][2] = (Timeto-Timefrom)/1e6;
         
		Timefrom = System.nanoTime();
		for(int i = 0; i < loop; i++)
			DoubleNegativeNumbersResult[i] = (double)Math.pow(DoubleNegativeNumbers[i], Powerof);
		Timeto = System.nanoTime();
		TimeResult[2][3] = (Timeto-Timefrom)/1e6;
        
         
		// Print Result
		System.out.println("Approach |  Case 1  |  Case 2  |  Case 3  |  Case 4");
		for(int i = 0; i < 3; i++)
			System.out.println("   " + (i+1) + "     | " + TimeResult[i][0] + " | " + TimeResult[i][1] + " | " + TimeResult[i][2] + " | " + TimeResult[i][3] + "");
	}
 

	static void powint(int n, int p) {
 		if(p <= 0)
			return;
		else if(n == 0 && p >= 1) { 
			resultint = 0;
			return;
		}
		else
			resultint = resultint * n;
 		powint(n, p-1);
	}

	static void powdouble(double n, int p) {
 		if(p <= 0)
			return;
		else if(n == 0 && p >= 1) { 
			resultdouble = 0;
			return;
		}
		else
			resultdouble = resultdouble * n;
		powdouble(n, p-1);
	}

}
